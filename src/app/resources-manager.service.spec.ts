import { TestBed, inject } from '@angular/core/testing';

import { ResourcesManagerService } from './resources-manager.service';

describe('ResourcesManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResourcesManagerService]
    });
  });

  it('should be created', inject([ResourcesManagerService], (service: ResourcesManagerService) => {
    expect(service).toBeTruthy();
  }));
});
