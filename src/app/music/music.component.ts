import { Component, OnInit } from '@angular/core';
import { ActivatorService, Activables } from '../activator.service';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeResourceUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
import { CoefficientsManagerService, Coefficients, Coefficient } from '../coefficients-manager.service';
import { FatherTimeService } from '../father-time.service';
import { UserAction, ActionsBuilderService } from '../actions-builder.service';
import { Resources, ResourcesManagerService } from '../resources-manager.service';
import { Statistics } from '../statistics-manager.service';
import { GameLoggerService } from '../game-logger.service';

export enum StageStatus {
  EMPTY,
  PLAYING
}

export class Stage {
  constructor(
    public readonly coefficient : Coefficients,
    private readonly coefficients : CoefficientsManagerService,
    private readonly gameLogger : GameLoggerService,
    private readonly resourcesManager : ResourcesManagerService,
    private readonly fatherTime : FatherTimeService,
    public status : StageStatus = StageStatus.EMPTY,
    public remainingTime : number = 0,
    public playingGenre : Genre = null
  ) {}

  onClick(selectedGenre : Genre) {
    if (selectedGenre && this.status === StageStatus.EMPTY) {
      this.gameLogger.logAction(selectedGenre.getGenreName());

      const concertDuration = this.coefficients.get(Coefficients.CONCERT_DURATION);
      this.playingGenre = selectedGenre;
      this.remainingTime = concertDuration;
      this.status = StageStatus.PLAYING;

      this.coefficients.set(this.coefficient, selectedGenre.getGenreName());
      // TODO: check if availables
      const dailyGroupies = this.coefficients.get(Coefficients.DAILY_GROUPIES);
      const cashNeeded = 1000 - (dailyGroupies / ​(200.0 + dailyGroupies) * 1000);
      this.resourcesManager.decrease(Resources.CASH, cashNeeded);

      this.fatherTime.schedule(
        () => { this.remainingTime -= 1; },
        1,
        concertDuration
      );
      this.fatherTime.schedule(
        () => {
          this.coefficients.set(this.coefficient, '');
          this.status = StageStatus.EMPTY;
        },
        concertDuration,
        1
      );
    }
  }
}

export enum Genres {
  POP = 'Indie/Pop',
  DEPRE = 'Indie/Depre',
  ACID_STONER = 'Underground',
  ROCK = 'Rock',
  REGGAE_SKA = 'Reggae/Ska',
  FOLK = 'Folk',
  HAPPY = 'Indie/Nonsense',
  PSYCH = 'Psych'
}

export class Genre {

  constructor(
    private genreName : Genres,
    private musicName : string,
    private playlistName : string,
    private coefficient : Coefficients,
    private playlistURL : SafeResourceUrl,
    private cm : CoefficientsManagerService
  ) {}

  getMusicName() {
    return this.musicName;
  }

  getGenreName() {
    return this.genreName;
  }

  getCoefficient() {
    return this.coefficient;
  }

  getPlaylistURL() {
    return this.playlistURL;
  }

  downAction() {
    if (this.cm.get(this.coefficient) >= 0.01) {
      this.cm.decrease(this.coefficient, 0.01);
    }
  }

  upAction() {
    if (this.cm.get(Coefficients.SUM_OF_PREFERENCES) <= 0.99) {
      this.cm.increase(this.coefficient, 0.01);
    }
  }
}

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {

  public Activables = Activables;
  public StageStatus = StageStatus;
  public Coefficients = Coefficients;

  private readonly defaultPlaylist =
    'https://embed.spotify.com/?uri=spotify:user:u69ewpoy0xqjgdd1baq2opv9m:playlist:23QWZeKdnLPVgXNIRuNzlq';

  public firstStage = new Stage(
    Coefficients.FIRST_STAGE_NOW_PLAYING,
    this.coefficients, this.gameLogger, this.resourcesManager, this.fatherTime
  );
  public secondStage = new Stage(
    Coefficients.SECOND_STAGE_NOW_PLAYING,
    this.coefficients, this.gameLogger, this.resourcesManager, this.fatherTime
  );
  public thirdStage = new Stage(
    Coefficients.THIRD_STAGE_NOW_PLAYING,
    this.coefficients, this.gameLogger, this.resourcesManager, this.fatherTime
  );
  public fourthStage = new Stage(
    Coefficients.FOURTH_STAGE_NOW_PLAYING,
    this.coefficients, this.gameLogger, this.resourcesManager, this.fatherTime
  );

  public volumeUpAction : UserAction;
  public volumeDownAction : UserAction;

  public listOfGenres = [
    new Genre(Genres.POP, 'indie pop', 'Musica per Limoni',
      Coefficients.POP_PREFERENCE, this.getPlaylistURL('4kRa6Kam8KH5pAjolmIGrm'), this.coefficients
    ),
    new Genre(Genres.FOLK, 'folk', 'Lemon Tree',
      Coefficients.FOLK_PREFERENCE, this.getPlaylistURL('39TB9mGf1f06gseqJSwOQz'), this.coefficients
    ),
    new Genre(Genres.ROCK, 'rock', 'Make Lemons Rock Again',
      Coefficients.ROCK_PREFERENCE, this.getPlaylistURL('40MIt7D1QHw379CgqwPrSO'), this.coefficients
    ),
    new Genre(Genres.ACID_STONER, 'underground', 'Acid Rock',
      Coefficients.UNDERGROUND_PREFERENCE, this.getPlaylistURL('20oeciqOfwUEbtgelaycjC'), this.coefficients
    ),
    new Genre(Genres.HAPPY, 'demenziale', 'Un limone, mezzo limone, due limoni',
      Coefficients.HAPPY_PREFERENCE, this.getPlaylistURL('4UoTEKAF3IDEXAaOxLbJxs'), this.coefficients
    ),
    new Genre(Genres.DEPRE, 'depre indie', 'When Life Gives you Lemon',
      Coefficients.DEPRE_PREFERENCE, this.getPlaylistURL('0HZVfpZipu5ZgyIELN00Gs'), this.coefficients
    ),
    new Genre(Genres.PSYCH, 'psichedelica', 'Lemons in the Sky with Diamonds',
      Coefficients.PSYCH_PREFERENCE, this.getPlaylistURL('4qiVaQx8JraIAdqHGrOpwb'), this.coefficients
    ),
    new Genre(Genres.REGGAE_SKA, 'reggae/ska', 'Smoked Lemons',
      Coefficients.REGGAE_PREFERENCE, this.getPlaylistURL('2sNhDIFfMjQdlbzXvEke68'), this.coefficients
    )
  ];

  public nowPlaying;

  constructor(
    // TODO: remove unused imports
    public activator : ActivatorService,
    private coefficients : CoefficientsManagerService,
    private resourcesManager : ResourcesManagerService,
    private fatherTime : FatherTimeService,
    private actionsBuilder : ActionsBuilderService,
    private gameLogger : GameLoggerService,
    private sanitizer : DomSanitizer
  ) {

    this.nowPlaying = this.sanitizer.bypassSecurityTrustResourceUrl(this.defaultPlaylist);

    this.volumeUpAction = this.actionsBuilder.create({
      title: '➕',
      activatedBy: Activables.VOLUME_UP,
      neededResources : [],
      coefficientsBoost: [
        { coefficient: Coefficients.DAILY_CUSTOMERS, adder: 8 },
        { coefficient: Coefficients.MUSIC_VOLUME, adder: 1 }
      ]
    });

    this.volumeDownAction = this.actionsBuilder.create({
      title: '➖',
      activatedBy: Activables.VOLUME_DOWN,
      neededResources : [],
      statsPreconditions : [{ stat: Statistics.MUSIC_VOLUME, howMuch : 6 }],
      coefficientsBoost: [
        { coefficient: Coefficients.DAILY_CUSTOMERS, adder: -8 },
        { coefficient: Coefficients.MUSIC_VOLUME, adder: -1 }
      ]
    });
  }

  ngOnInit() {

    // Artistic director
    this.fatherTime.schedule(() => {
      if (!this.activator.check(Activables.AUTOMATIC_CONCERTS)) {
        return;
      }

      [
        this.firstStage,
        this.secondStage,
        this.thirdStage,
        this.fourthStage
      ]
      .filter(stage => stage.status === StageStatus.EMPTY)
      .forEach(stage => {
        const chosenGenre : Genre = this.extractGenre();
        if (chosenGenre && Math.random() < 0.33 /* adds some randomness to the stages */) {
          stage.onClick(chosenGenre);
          console.log(chosenGenre.getGenreName());
        }
      });
    }, 2);
  }

  isMobile() : boolean {
    return (Math.max(window.screen.height, window.screen.width) <= 1024);
  }

  getPlaylist(playableGenre) {
    if (playableGenre) {
      return playableGenre.getPlaylistURL();
    } else {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.defaultPlaylist);
    }
  }

  playIfMobile(playableGenre) {
    if (this.isMobile()) {
      this.nowPlaying = this.getPlaylist(playableGenre);
    }
  }

  private extractGenre() {
    const random = Math.random();
    let accumulator = 0;
    let chosenGenre : Genre = null;
    this.listOfGenres.forEach(genre => {
      accumulator += this.coefficients.get(genre.getCoefficient());
      if (!chosenGenre && random < accumulator) {
        chosenGenre = genre;
      }
    });
    return chosenGenre;
  }

  private getPlaylistURL(playlistID : string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://embed.spotify.com/?uri=spotify:user:u69ewpoy0xqjgdd1baq2opv9m:playlist:'
      + playlistID
    );
  }
}
