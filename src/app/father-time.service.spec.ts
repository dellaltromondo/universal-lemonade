import { TestBed, inject } from '@angular/core/testing';

import { FatherTimeService } from './father-time.service';

describe('FatherTimeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FatherTimeService]
    });
  });

  it('should be created', inject([FatherTimeService], (service: FatherTimeService) => {
    expect(service).toBeTruthy();
  }));
});
