import { TestBed, inject } from '@angular/core/testing';

import { StatisticsManagerService } from './statistics-manager.service';

describe('StatisticsManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StatisticsManagerService]
    });
  });

  it('should be created', inject([StatisticsManagerService], (service: StatisticsManagerService) => {
    expect(service).toBeTruthy();
  }));
});
