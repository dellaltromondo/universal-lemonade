import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheatBoxComponent } from './cheat-box.component';

describe('CheatBoxComponent', () => {
  let component: CheatBoxComponent;
  let fixture: ComponentFixture<CheatBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheatBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheatBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
