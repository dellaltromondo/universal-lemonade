import { Component, OnInit, Inject } from '@angular/core';
import { CoefficientsManagerService, Coefficients } from '../coefficients-manager.service';
import { ResourcesManagerService, Resources } from '../resources-manager.service';
import { ActivatorService, Activables } from '../activator.service';
import { StatisticsManagerService, Statistics } from '../statistics-manager.service';
import { ActionsBuilderService } from '../actions-builder.service';
import { Injector } from '@angular/core';

@Component({
  selector: 'app-enhancements-panel',
  templateUrl: './enhancements-panel.component.html',
  styleUrls: ['./enhancements-panel.component.css']
})
export class EnhancementsPanelComponent implements OnInit {

  public Activables = Activables;
  public Resources = Resources;
  public Statistics = Statistics;

  public listOfEnhancements = [];

  constructor(
    private coefficientsManager : CoefficientsManagerService,
    public resourcesManager : ResourcesManagerService,
    public activator : ActivatorService,
    private stats : StatisticsManagerService,
    private actionsBuilder : ActionsBuilderService
  ) {

    this.listOfEnhancements = [

      this.actionsBuilder.create({
        title: 'Nuova scoperta: spremiagrumi',
        subTitle: 'Migliora la tua capacità di spremitura',
        description: 'Raddoppia la quantità di succo spremuto da ogni limone',
        activatedBy: Activables.SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 3 } ],
        unlocks: [
          Activables.BETTER_SQUEEZER,
          Activables.RADIO,
          Activables.LAB_BLOCK,
          Activables.BOTTLE_STORAGE
        ],
        coefficientsBoost: [ { coefficient : Coefficients.LEMON_SQUEEZABILITY, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi migliorato',
        description: 'Raddoppia la quantità di succo spremuto da ogni limone',
        activatedBy: Activables.BETTER_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 10 } ],
        unlocks: [
          Activables.EFFICIENT_SQUEEZER,
          Activables.AUTOMATIC_SQUEEZER
        ],
        coefficientsBoost: [{ coefficient : Coefficients.LEMON_SQUEEZABILITY, multiplier : 2 }]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi efficiente',
        description: 'Raddoppia la quantità di succo spremuto da ogni limone',
        activatedBy: Activables.EFFICIENT_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 20 } ],
        unlocks: [ Activables.TOP_SQUEEZER, Activables.POSTERS_MARKETING ],
        coefficientsBoost: [ { coefficient : Coefficients.LEMON_SQUEEZABILITY, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi 🔝',
        description: 'Raddoppia la quantità di succo spremuto da ogni limone',
        activatedBy: Activables.TOP_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 100 } ],
        statsPreconditions: [],
        unlocks: [ Activables.STORE_MANAGER, Activables.FAST_SQUEEZER ],
        coefficientsBoost: [ { coefficient : Coefficients.LEMON_SQUEEZABILITY, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi rotante',
        description: 'Raddoppia la velocità di spremitura',
        activatedBy: Activables.FAST_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 550 } ],
        statsPreconditions: [],
        unlocks: [ Activables.ENORMOUS_POSTERS ],
        coefficientsBoost: [ { coefficient : Coefficients.SQUEEZER_QUICKNESS, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi assassino',
        description: 'Quadruplica la velocità di spremitura',
        activatedBy: Activables.FASTER_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 3000 } ],
        statsPreconditions: [],
        unlocks: [ ],
        coefficientsBoost: [ { coefficient : Coefficients.SQUEEZER_QUICKNESS, multiplier : 4 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi ipersonico',
        description: 'Quadruplica la velocità di spremitura',
        activatedBy: Activables.HYPERSONIC_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 55000 } ],
        statsPreconditions: [],
        unlocks: [ Activables.QUANTIC_SQUEEZER, Activables.BIKE_PROMOTION ],
        coefficientsBoost: [ { coefficient : Coefficients.SQUEEZER_QUICKNESS, multiplier : 4 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Promozione sull\'uso della bicicletta',
        description: 'Dimezza la quantità di CO2 emessa',
        activatedBy: Activables.BIKE_PROMOTION,
        neededResources: [ { resource : Resources.CASH, howMuch : 25000 } ],
        statsPreconditions: [ { stat : Statistics.SOLD_SOPHISTICATED_LEMONADE, howMuch: 300 } ],
        unlocks: [ Activables.RECYCLING ],
        coefficientsBoost: [ { coefficient : Coefficients.CO2_PRODUCTION, multiplier : 0.2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Raccolta differenziata',
        description: 'Dimezza la quantità di CO2 emessa',
        activatedBy: Activables.RECYCLING,
        neededResources: [ { resource : Resources.CASH, howMuch : 45000 } ],
        statsPreconditions: [],
        unlocks: [ Activables.COMPOSTABLES ],
        coefficientsBoost: [ { coefficient : Coefficients.CO2_PRODUCTION, multiplier : 0.2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Prodotti compostabili',
        description: 'Dimezzano la quantità di CO2 emessa riducendo l\'utilizzo di plastica',
        activatedBy: Activables.COMPOSTABLES,
        neededResources: [ { resource : Resources.CASH, howMuch : 135000 } ],
        statsPreconditions: [],
        unlocks: [ Activables.REUSABLE_GLASSES ],
        coefficientsBoost: [ { coefficient : Coefficients.CO2_PRODUCTION, multiplier : 0.2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Bicchieri riutilizzabili',
        description: 'Bicchieri di plastica dura con logo figo, dimezzano la quantità di CO2 emessa',
        activatedBy: Activables.REUSABLE_GLASSES,
        neededResources: [ { resource : Resources.CASH, howMuch : 230000 } ],
        coefficientsBoost: [ { coefficient : Coefficients.CO2_PRODUCTION, multiplier : 0.2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi quantistico',
        description: 'Centuplica la velocità di spremitura',
        activatedBy: Activables.QUANTIC_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 1750000 } ],
        statsPreconditions: [ { stat: Statistics.SOLD_SOPHISTICATED_LEMONADE, howMuch: 200000 } ],
        unlocks: [ Activables.BLOCKCHAIN_SQUEEZER ],
        coefficientsBoost: [ { coefficient : Coefficients.SQUEEZER_QUICKNESS, multiplier : 100 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi in blockchain',
        description: 'Centuplica la velocità di spremitura',
        activatedBy: Activables.BLOCKCHAIN_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 9000000 } ],
        statsPreconditions: [{ stat: Statistics.REPUTATION, howMuch: 950 }],
        unlocks: [ Activables.CONVERTER_TECHNOLOGY ],
        coefficientsBoost: [ { coefficient : Coefficients.SQUEEZER_QUICKNESS, multiplier : 100 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Ufficio marketing',
        description: 'Studia manifesti per aumentare i clienti',
        activatedBy: Activables.POSTERS_MARKETING,
        neededResources: [ { resource : Resources.CASH, howMuch : 20 } ],
        statsPreconditions: [ { stat : Statistics.SOLD_LEMONADE, howMuch : 200 } ],
        unlocks: [ Activables.POSTERS, Activables.BIGGER_POSTERS, Activables.CHANGE_VOLUMES ],
        coefficientsBoost: []
      }, true),

      this.actionsBuilder.create({
        title: 'Manifesti più grandi',
        description: 'Raddoppia l\'effetto dei futuri manifesti',
        activatedBy: Activables.BIGGER_POSTERS,
        neededResources: [ { resource : Resources.CASH, howMuch : 200 } ],
        statsPreconditions: [],
        unlocks: [ ],
        coefficientsBoost: [ { coefficient: Coefficients.POSTER_SIZE, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Manifesti enormi',
        description: 'Raddoppia l\'effetto dei futuri manifesti',
        activatedBy: Activables.ENORMOUS_POSTERS,
        neededResources: [ { resource : Resources.CASH, howMuch : 700 } ],
        statsPreconditions: [],
        unlocks: [ Activables.FASTER_SQUEEZER ],
        coefficientsBoost: [ { coefficient: Coefficients.POSTER_SIZE, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Store manager',
        description: 'Archivia sempre più limonata!',
        activatedBy: Activables.STORE_MANAGER,
        neededResources: [ { resource : Resources.CASH, howMuch : 100 } ],
        statsPreconditions: [],
        unlocks: [ Activables.DEMIJOHN_STORAGE, Activables.TANK_STORAGE ],
        coefficientsBoost: []
      }, true),

      this.actionsBuilder.create({
        title: 'Più volume!',
        description: 'Potrai regolare il volume della musica',
        activatedBy: Activables.CHANGE_VOLUMES,
        neededResources: [ { resource : Resources.CASH, howMuch : 300 } ],
        statsPreconditions: [ { stat : Statistics.SOLD_LEMONADE, howMuch : 500 } ],
        unlocks: [
          Activables.VOLUME_UP,
          Activables.VOLUME_DOWN,
          Activables.REPUTATION,
          Activables.CHARITY
        ]
      }, true),

      this.actionsBuilder.create({
        title: 'Beneficenza',
        description: 'Dona una parte dei tuoi guadagni a chi ne ha bisogno e aumenta la tua reputazione',
        activatedBy: Activables.CHARITY,
        neededResources: [{ resource : Resources.CASH, howMuch : 6000 }],
        resourcesBoost: [{ resource : Resources.REPUTATION, howMuch: 50 }],
        unlocks: [ Activables.CHARITY_2 ]
      }, true),

      this.actionsBuilder.create({
        title: 'Beneficenza sostanziosa',
        description: 'Dona una parte dei tuoi guadagni a chi ne ha bisogno e aumenta la tua reputazione',
        activatedBy: Activables.CHARITY_2,
        neededResources: [ { resource : Resources.CASH, howMuch : 45000 } ],
        resourcesBoost: [{ resource : Resources.REPUTATION, howMuch: 100 }],
        unlocks: [ Activables.CHARITY_3 ]
      }, true),

      this.actionsBuilder.create({
        title: 'Beneficenza filantropica',
        description: 'Dona una parte dei tuoi guadagni a chi ne ha bisogno e aumenta la tua reputazione',
        activatedBy: Activables.CHARITY_3,
        neededResources: [ { resource : Resources.CASH, howMuch : 300000 } ],
        resourcesBoost: [{ resource : Resources.REPUTATION, howMuch: 150 }],
        unlocks: [ ]
      }, true),

      // #### "Stages" level ####

      this.actionsBuilder.create({
        title: 'Gestione di eventi',
        subTitle: 'Richiede la quantità massima di manifesti',
        description: 'Sei pronto a lanciare il tuo festival?',
        activatedBy: Activables.EVENT_MANAGEMENT,
        neededResources: [{ resource : Resources.CASH, howMuch : 35000 }],
        statsPreconditions: [
          { stat : Statistics.SOLD_LEMONADE, howMuch : 100000 },
          { stat : Statistics.POSTERS_LEVEL, howMuch : 9 }
        ],
        unlocks: [
          Activables.STAGES,
          Activables.BIG_EVENTS,
          Activables.BETTER_LIGHTS,
          Activables.CO2
        ],
      }, true),

      this.actionsBuilder.create({
        title: 'Eventi enormi',
        description: 'I palchi diventano quattro',
        activatedBy: Activables.BIG_EVENTS,
        neededResources: [{ resource : Resources.CASH, howMuch : 1000000 }],
        statsPreconditions: [
          { stat : Statistics.SOLD_LEMONADE, howMuch : 2000000 }
        ],
        unlocks: [
          Activables.MULTIPLE_STAGES,
          Activables.ARTISTIC_DIRECTOR
        ],
      }, true),

      this.actionsBuilder.create({
        title: 'Direzione artistica',
        description: 'Assumi una persona che si preoccuperà dei concerti',
        activatedBy: Activables.ARTISTIC_DIRECTOR,
        neededResources: [{ resource : Resources.CASH, howMuch : 1650000 }],
        statsPreconditions: [{ stat : Statistics.REPUTATION, howMuch : 900 }],
        unlocks: [ Activables.AUTOMATIC_CONCERTS ],
      }, true),

      this.actionsBuilder.create({
        title: 'Luci fighe',
        description: 'Mantiene il pubblico per più tempo',
        activatedBy: Activables.BETTER_LIGHTS,
        neededResources: [{ resource : Resources.CASH, howMuch : 45000 }],
        coefficientsBoost: [ { coefficient: Coefficients.ESCAPE_RATE, adder: -0.04 } ],
        unlocks: [
          Activables.MARKET,
          Activables.BETTER_ARTISTS
        ],
      }, true),

      this.actionsBuilder.create({
        title: 'Impianto godurioso',
        description: 'Mantiene il pubblico per più tempo',
        activatedBy: Activables.BETTER_SOUNDS,
        neededResources: [{ resource : Resources.CASH, howMuch : 135000 }],
        statsPreconditions: [{ stat: Statistics.SOLD_SOPHISTICATED_LEMONADE, howMuch: 20000}],
        coefficientsBoost: [ { coefficient: Coefficients.ESCAPE_RATE, adder: -0.04 } ],
        unlocks: [ Activables.EVEN_BETTER_ARTISTS ],
      }, true),

      this.actionsBuilder.create({
        title: 'Artisti navigati',
        description: 'Aumenta la durata dei concerti',
        activatedBy: Activables.BETTER_ARTISTS,
        neededResources: [{ resource : Resources.CASH, howMuch : 270000 }],
        statsPreconditions: [{ stat: Statistics.REPUTATION, howMuch : 650 }],
        coefficientsBoost: [ { coefficient: Coefficients.CONCERT_DURATION, adder: 15 } ],
        unlocks: [ Activables.BETTER_SOUNDS, ],
      }, true),

      this.actionsBuilder.create({
        title: 'Artisti esperti',
        description: 'Aumenta la durata dei concerti',
        activatedBy: Activables.EVEN_BETTER_ARTISTS,
        neededResources: [{ resource : Resources.CASH, howMuch : 500000 }],
        coefficientsBoost: [ { coefficient: Coefficients.CONCERT_DURATION, adder: 15 } ],
        unlocks: [ Activables.FLEA_MARKET ],
      }, true),

      this.actionsBuilder.create({
        title: 'Bancarelle',
        description: 'Aumentano l\'attrattività del tuo festival',
        activatedBy: Activables.MARKET,
        neededResources: [ { resource : Resources.CASH, howMuch : 80000 } ],
        statsPreconditions: [ { stat : Statistics.REPUTATION, howMuch: 450 } ],
        coefficientsBoost: [ { coefficient: Coefficients.FESTIVAL_ATTRACTIVENESS, adder: 10 } ],
        unlocks: [ Activables.HYPERSONIC_SQUEEZER ]
      }, true),

      this.actionsBuilder.create({
        title: 'Mercatino dell\'usato',
        description: 'Aumenta l\'attrattività del tuo festival',
        activatedBy: Activables.FLEA_MARKET,
        neededResources: [ { resource : Resources.CASH, howMuch : 250000 } ],
        statsPreconditions: [ { stat : Statistics.REPUTATION, howMuch: 750 } ],
        coefficientsBoost: [ { coefficient: Coefficients.FESTIVAL_ATTRACTIVENESS, adder: 20 } ],
        unlocks: [ Activables.VINTAGE_MARKET ]
      }, true),

      this.actionsBuilder.create({
        title: 'Mercatino vintage',
        description: 'Aumenta l\'attrattività del tuo festival',
        activatedBy: Activables.VINTAGE_MARKET,
        neededResources: [ { resource : Resources.CASH, howMuch : 1500000 } ],
        statsPreconditions: [
          { stat : Statistics.REPUTATION, howMuch: 900 },
          { stat : Statistics.SOLD_SOPHISTICATED_LEMONADE, howMuch : 150000 }
        ],
        coefficientsBoost: [ { coefficient: Coefficients.FESTIVAL_ATTRACTIVENESS, adder: 40 } ],
        unlocks: [ Activables.SILOS_STORAGE ]
      }, true),

      // #### "Save the world" level ####

      this.actionsBuilder.create({
        title: 'Convertitore a base acida',
        subTitle: 'Sarai tu il futuro Elon Musk?',
        description: 'Una nuova tecnologia permette di ridurre la CO2 presente nell\'aria utilizzando limonata',
        activatedBy: Activables.CONVERTER_TECHNOLOGY,
        neededResources: [{ resource : Resources.CASH, howMuch : 20000000 }],
        statsPreconditions: [
          { stat : Statistics.REPUTATION, howMuch : 1000 },
          { stat : Statistics.SOLD_LEMONADE, howMuch : 20000000 },
          { stat : Statistics.SOLD_SOPHISTICATED_LEMONADE, howMuch : 5000000 },
        ],
        unlocks: [
          Activables.CONVERTER, Activables.NEURAL_SQUEEZER
        ],
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi neurale',
        description: 'Raddoppia la velocità di spremitura',
        activatedBy: Activables.NEURAL_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 15000000 } ],
        statsPreconditions: [],
        unlocks: [ Activables.SELF_LEARNING_SQUEEZER ],
        coefficientsBoost: [ { coefficient : Coefficients.SQUEEZER_QUICKNESS, multiplier : 2 } ]
      }, true),

      this.actionsBuilder.create({
        title: 'Spremiagrumi cosciente',
        subTitle: 'Uno spremiagrumi in grado di migliorare se stesso',
        description: 'Raddoppia la velocità di spremitura ogni minuto',
        activatedBy: Activables.SELF_LEARNING_SQUEEZER,
        neededResources: [ { resource : Resources.CASH, howMuch : 50000000 } ],
        unlocks: [ ],
        toBeScheduled : [{
          action : () => {
            this.coefficientsManager.multiply(Coefficients.SQUEEZER_QUICKNESS, 2);
          },
          executeEvery : 60,
        }]
      }, true)
    ];
  }

  ngOnInit() {
  }
}
