import { Injectable } from '@angular/core';

// TODO: move elsewhere (config file for game)
export enum Resources {
  // These are string enums, so that we can save and load them easily
  LEMONS = 'LEMONS',
  LITERS_OF_LEMONADE = 'LITERS_OF_LEMONADE',
  CASH = 'CASH',
  AUTOMATIC_SQUEEZER = 'AUTOMATIC_SQUEEZER',
  POSTERS = 'POSTERS',
  STORAGE_LITERS = 'STORAGE_LITERS',
  REPUTATION = 'REPUTATION',
  CONVERTERS = 'CONVERTERS',
  CO2 = 'CO2'
}

@Injectable()
export class ResourcesManagerService {

  public static readonly STARTING_LEMONS = 1000000000000;

  private storeHouse : Map<Resources, Number> = new Map<Resources, Number>();

  constructor() {
    for (const resource in Resources) {
      if (! (resource in this.storeHouse.keys())) {
        this.storeHouse[resource] = 0;
      }
    }
    this.storeHouse[Resources.LEMONS] = ResourcesManagerService.STARTING_LEMONS;
    this.storeHouse[Resources.STORAGE_LITERS] = 2;
    this.storeHouse[Resources.REPUTATION] = 500;
  }

  get(which : Resources) {
    return this.storeHouse[which];
  }

  increase(which : Resources, howMuch : Number = 1) {
    this.storeHouse[which] += howMuch;
  }

  decrease(which : Resources, howMuch : Number = 1) {
    this.increase(which, -howMuch);
  }

  theresEnough(which : Resources, howMuch : Number = 1) : boolean {
    return this.storeHouse[which] >= howMuch;
  }

  serialize() : string {
    return JSON.stringify(this.storeHouse);
  }

  initialize(json) {
    let newDictionary = json;
    if (typeof json === 'string') {
      newDictionary = JSON.parse(json);
    }
    for (const key in this.storeHouse) {
      if (key in newDictionary) {
        this.storeHouse[key] = newDictionary[key];
      }
    }
  }

  getLog() {
    return JSON.parse(JSON.stringify(this.storeHouse));
  }

  resourceToString(resource : Resources) : string {
    switch (resource) {
      case Resources.CASH:
        return '€';
      case Resources.AUTOMATIC_SQUEEZER:
        return 'spremitori automatici';
      case Resources.LEMONS:
        return 'limoni';
      case Resources.LITERS_OF_LEMONADE:
        return 'litri di limonata';
      case Resources.POSTERS:
        return 'livello di attacchinaggio';
      case Resources.STORAGE_LITERS:
        return 'litri di stoccaggio';
      default:
        return '-- risorsa senza versione stringa --';
    }
  }
}
