import { TestBed, inject } from '@angular/core/testing';

import { CoefficientsManagerService } from './coefficients-manager.service';

describe('CoefficientsManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoefficientsManagerService]
    });
  });

  it('should be created', inject([CoefficientsManagerService], (service: CoefficientsManagerService) => {
    expect(service).toBeTruthy();
  }));
});
