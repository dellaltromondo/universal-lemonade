import { TestBed, inject } from '@angular/core/testing';

import { ActivatorService } from './activator.service';

describe('ActivatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivatorService]
    });
  });

  it('should be created', inject([ActivatorService], (service: ActivatorService) => {
    expect(service).toBeTruthy();
  }));
});
