import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LemonadeStandComponent } from './lemonade-stand/lemonade-stand.component';
import { EnhancementsPanelComponent } from './enhancements-panel/enhancements-panel.component';

import { ResourcesManagerService } from './resources-manager.service';
import { CoefficientsManagerService } from './coefficients-manager.service';
import { FatherTimeService } from './father-time.service';
import { LaboratoryComponent } from './laboratory/laboratory.component';
import { MarketingComponent } from './marketing/marketing.component';
import { StatisticsManagerService } from './statistics-manager.service';
import { ActivatorService } from './activator.service';
import { MusicComponent } from './music/music.component';
import { CheatBoxComponent } from './cheat-box/cheat-box.component';
import { EnhancementButtonComponent } from './enhancement-button/enhancement-button.component';
import { ActionsBuilderService } from './actions-builder.service';
import { DecimalPipe, Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ActionButtonComponent } from './action-button/action-button.component';
import { GameLoggerService } from './game-logger.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TabsMenuComponent } from './tabs-menu/tabs-menu.component';
import { StageComponent } from './stage/stage.component';
import { ShortNumberPipe } from './short-number.pipe';
import { PickOnePipe } from './pick-one.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeadroomModule } from '@ctrl/ngx-headroom';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    LemonadeStandComponent,
    EnhancementsPanelComponent,
    LaboratoryComponent,
    MarketingComponent,
    MusicComponent,
    CheatBoxComponent,
    EnhancementButtonComponent,
    ActionButtonComponent,
    TabsMenuComponent,
    StageComponent,
    ShortNumberPipe,
    PickOnePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HeadroomModule,
    RouterModule
  ],
  providers: [
    ResourcesManagerService,
    CoefficientsManagerService,
    StatisticsManagerService,
    ActivatorService,
    FatherTimeService,
    ActionsBuilderService,
    GameLoggerService,
    DecimalPipe,
    HttpClient,
    Location,
    {provide: LocationStrategy, useClass: PathLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
