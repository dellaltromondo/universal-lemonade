import { Component, OnInit } from '@angular/core';
import { ActivatorService, Activables } from '../activator.service';
import { CoefficientsManagerService, Coefficients } from '../coefficients-manager.service';
import { ResourcesManagerService, Resources } from '../resources-manager.service';
import { StatisticsManagerService, Statistics } from '../statistics-manager.service';
import { GameLoggerService } from '../game-logger.service';
import { FatherTimeService } from '../father-time.service';

interface Save {
  name : string;
  activables : string;
  coefficients : string;
  resources : string;
  stats : string;
}

@Component({
  selector: 'app-cheat-box',
  templateUrl: './cheat-box.component.html',
  styleUrls: ['./cheat-box.component.css']
})
export class CheatBoxComponent implements OnInit {

  private readonly saveKey : string = 'UNIVERSAL_LEMONADE_SAVES';
  public saves : Save[] = [];
  public Resources = Resources;
  public Coefficients = Coefficients;
  public Activables = Activables;
  public Statistics = Statistics;

  constructor(
    public activator : ActivatorService,
    public coefficients : CoefficientsManagerService,
    public resources : ResourcesManagerService,
    public stats : StatisticsManagerService,
    public fatherTime : FatherTimeService
  ) {
    const savesString = localStorage.getItem(this.saveKey);
    if (savesString !== null) {
      this.saves = JSON.parse(savesString);
    }
  }

  ngOnInit() {}

  saveState() {
    let saveName = '';
    do {
      saveName = prompt('Inserisci il nome del salvataggio');
    } while (saveName === '');

    console.log(this.activator.serialize());
    console.log(this.coefficients.serialize());
    console.log(this.resources.serialize());
    console.log(this.stats.serialize());

    const newSave : Save = {
      name : saveName,
      activables : this.activator.serialize(),
      coefficients : this.coefficients.serialize(),
      resources : this.resources.serialize(),
      stats : this.stats.serialize()
    };
    this.saves.push(newSave);
    localStorage.setItem(this.saveKey, JSON.stringify(this.saves));
  }

  loadState(saveIndex : number) {
    const save : Save = this.saves[saveIndex];
    this.activator.initialize(save.activables);
    this.coefficients.initialize(save.coefficients);
    this.resources.initialize(save.resources);
    this.stats.initialize(save.stats);
  }

  deleteSave(saveIndex : number) {
    if (confirm('Sei sicuro di voler eliminare il salvataggio "' + this.saves[saveIndex].name + '"?')) {
      this.saves.splice(saveIndex, 1);
      localStorage.setItem(this.saveKey, JSON.stringify(this.saves));
    }
  }

  resetGame() {
    localStorage.setItem(GameLoggerService.gameSaveKey, '');
    localStorage.setItem(GameLoggerService.guidSaveKey, '');
    localStorage.setItem(GameLoggerService.nameSaveKey, '');
    console.log('Game reset!');
  }
}
