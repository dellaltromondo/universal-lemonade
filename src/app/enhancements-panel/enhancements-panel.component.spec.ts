import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnhancementsPanelComponent } from './enhancements-panel.component';

describe('EnhancementsPanelComponent', () => {
  let component: EnhancementsPanelComponent;
  let fixture: ComponentFixture<EnhancementsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnhancementsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnhancementsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
